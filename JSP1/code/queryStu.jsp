<%@ page language="java" import="java.util.*,java.sql.*" 
         contentType="text/html; charset=utf-8"
%><%
	request.setCharacterEncoding("utf-8");
	String msg ="";
	
	String connectString = "jdbc:mysql://localhost:53306/teaching5"
					+ "?autoReconnect=true&useUnicode=true"
					+ "&characterEncoding=UTF-8"; 
    StringBuilder table=new StringBuilder("");
	String query = request.getParameter("query");
	try{
	  Class.forName("com.mysql.jdbc.Driver");
	  Connection con=DriverManager.getConnection(connectString, 
	                 "user", "123");
	  String sql;
	  Statement stmt=con.createStatement();
	  if(request.getMethod().equalsIgnoreCase("post")){
		  sql="select * from stu where num like '%"+query+"%' or name like '%"+query+"%' order by num";
	  }else{
		  sql="select * from stu";
	  }
	  ResultSet rs=stmt.executeQuery(sql);
	  table.append("<table><tr><th>id</th><th>学号</th><th>姓名</th>"+"<th>-</th></tr>");
	  while(rs.next()){
		  table.append(String.format(
					"<tr><td>%s</td><td>%s</td><td>%s</td><td>%s %s</td></tr>",
					rs.getString("id"),rs.getString("num"),rs.getString("name"),
					"<a href='updateStu2013.jsp?pid="+rs.getString("id")+" '>修改</a>",
					"<a href='deleteStu2013.jsp?pid="+rs.getString("id")+" '>删除</a>")
			);
	  }
	  table.append("</table>");
	  rs.close();
	  stmt.close();
	  con.close();
	}
	catch (Exception e){
	  msg = e.getMessage();
	}
	if(query==null) query="";
%>
<!DOCTYPE HTML>
<html>
<head>
<title>反页浏览学生名单</title>
<style>
	table{
		border-collapse:collapse;
		border:none;
		width:500px;
	}
	td,th{
		border:solid grey 1px;
		margin:0 0 0 0;
		padding:5px 5px 5px 5px;
	}
	a:link,a:visited{
		color:blue;
	}
	.container{
		margin:0 auto;
		width:500px;
		text-align:center;
	}
</style>
</head>
<body>
  <div class="container">
	  <h1>查询学生名单</h1>
	  <form action="queryStu2013.jsp" method="post" name="f">
			输入查询:<input id="query" name="query" type="text" value="<%=query%>">
			<input name="sub" type="submit" value="查询">
			<br></br>
	  </form>
	  <%=table%>
	  <br><br>
	  <div style="float:left">
			<a href="addStu2013.jsp">新增</a>
			<a href="browseStu22013.jsp">返回</a>
	  </div>
	  <br><br>
	  <%=msg%><br><br>
  </div>
</body>
</html>


