import java.util.*;
import java.sql.*;

public class QueryStu {
	static Scanner in = new Scanner(System.in);
	static int sno = 1;
	static private Connection conn;
	public static void main(String args[]) {
        String line;
		String query;
		if(connect()){
			while ((line=getNextLine())!=null) { // 是否还有输入
				if(line.charAt(0)=='-'){
					query = "select * from stu where id=" + line.substring(1,line.length());
				}else if(line.charAt(0)=='*'){
					query = "select * from stu order by num";
				}else{
					query = "select * from stu WHERE num LIKE '%"+line+"%' OR name LIKE '%"+line+"%' ORDER BY num";
				}
				ResultSet rs = executeQuery(query);
				int count = 0;
				try{
					while(rs.next()){
						count++;
						System.out.print(rs.getString("id")+" ");	
						System.out.print(rs.getString("num")+" ");
						System.out.println(rs.getString("name"));							
					}
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
				System.out.println("["+count+"条记录]\n");
			} 
		}
	}
	public static String getNextLine(){
		if (sno==1){
			System.out.println("查询学号和姓名（部分匹配），以-开头查询id，*查询所有结果，exit或空行退出.\r\n");
		}
		System.out.print(sno+"> ");			
		if(!in.hasNextLine()) { // 是否还有输入
			return null;
		}
		String line = in.nextLine(); // 读取下一行
		line = line.trim();
		if (line.equals("exit") || line.length() == 0) {
			return null;
		}
		sno++;
		return line;
	}
	private static boolean connect() {
		String connectString = "jdbc:mysql://172.18.187.231:53306/teaching"
				+ "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&&useSSL=false";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(connectString, "user", "123");
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	static private ResultSet executeQuery(String sqlSentence) {
	     Statement stat;
	     ResultSet rs = null;
	     try {
			stat = conn.createStatement();       //获取执行sql语句的对象
			rs = stat.executeQuery(sqlSentence); //执行sql查询，返回结果集
	     } catch (Exception e) {
			System.out.println(e.getMessage());
	     }
	     return rs;
	}
}