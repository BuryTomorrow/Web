import java.util.*;
import java.sql.*;

public class UpdateStu {
	static Scanner in = new Scanner(System.in);
	static int sno = 1;
	static private Connection conn;
	public static void main(String args[]) {
        String lineWords[];
		String query;
		if(connect()){
			while ((lineWords=getNextLineWords())!=null) { // 是否还有输入
				if(lineWords.length == 3){
					query = "UPDATE stu SET num='"+lineWords[1]+"', name='"+lineWords[2]+"'  WHERE id="+lineWords[0];
				}else{
					query = "UPDATE stu SET name='"+lineWords[1]+"'  WHERE num= '"+lineWords[0]+"'";
				}
				executeQuery(query);
			}
		}
	}
	public static String[] getNextLineWords(){
		if (sno==1){
			System.out.println("输入学号、姓名或者id、学号、姓名（用空格间隔），exit或空行退出.\r\n");
		}
		System.out.print(sno+"> ");			
		if(!in.hasNextLine()) { // 是否还有输入
			return null;
		}
		String line = in.nextLine(); // 读取下一行
		line = line.trim();
		if (line.equals("exit") || line.length() == 0) {
			return null;
		}
		sno++;
		return line.split(" ");	
	}
	private static boolean connect() {
		String connectString = "jdbc:mysql://172.18.187.231:53306/teaching"
				+ "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&&useSSL=false";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(connectString, "user", "123");
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	static private void executeQuery(String sqlSentence) {
	     Statement stat;
	     try {
			stat = conn.createStatement();
			stat.executeUpdate(sqlSentence);
			System.out.println("1条记录已被修改!\n");
	     } catch (Exception e) {
			System.out.println(e.getMessage());
			System.out.println("0条记录已被修改!\n");
	     }
	}
}