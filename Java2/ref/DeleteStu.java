import java.util.*;
import java.sql.*;

public class DeleteStu {
	static Scanner in = new Scanner(System.in);
	static int sno = 1;
	static private Connection conn;
	public static void main(String args[]) {
        String line;
		String query;
		if(connect()){
			while ((line=getNextLine())!=null) { // 是否还有输入
				if(line.charAt(0)=='-'){
					query = "DELETE  FROM stu WHERE  id=" + line.substring(1,line.length());
				}else{
					query = "DELETE  FROM stu WHERE num LIKE '%"+line+"%'  OR name LIKE '%"+line+"%'";
				}
				executeQuery(query);
			} 
		}
	}
	public static String getNextLine(){
		if (sno==1){
			System.out.println("输入学号或姓名（部分匹配）以删除记录，输入以-开头数字则按id删除，exit或空行退出.\r\n");
		}
		System.out.print(sno+"> ");			
		if(!in.hasNextLine()) { // 是否还有输入
			return null;
		}
		String line = in.nextLine(); // 读取下一行
		line = line.trim();
		if (line.equals("exit") || line.length() == 0) {
			return null;
		}
		sno++;
		return line;
	}
	private static boolean connect() {
		String connectString = "jdbc:mysql://172.18.187.231:53306/teaching"
				+ "?autoReconnect=true&useUnicode=true&characterEncoding=UTF-8&&useSSL=false";
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(connectString, "user", "123");
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	static private void executeQuery(String sqlSentence) {
	     Statement stat;
	     try {
			stat = conn.createStatement();       //获取执行sql语句的对象
			int a=stat.executeUpdate(sqlSentence); //执行sql查询，返回结果集
			System.out.println(a+"个记录被删除\n");
	     } catch (Exception e) {
			System.out.println(e.getMessage());
	     }
	}
}