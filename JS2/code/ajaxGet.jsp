﻿<!DOCTYPE html>
<html>
<head>
  <title>AJAX-get</title>
  <style>
     p {font-size:20px}
   </style>
</head>
<body>
  <h1>Ajax-get</h1>
  <p>id：<input id="t1" type="text"></p>
  <p id="t2">[无]</p>
  <script>
      var input = document.getElementById("t1");
      input.onblur = function () {
      	  var oTest = document.getElementById("t2");
          oTest.innerHTML = "正在查询...";
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function () {
              if (xmlhttp.readyState == 4) {
              	  
                  if (xmlhttp.status >= 200 && xmlhttp.status < 300 || xmlhttp.status == 304) {
                      alert(xmlhttp.responseText);
                      oTest.innerHTML = xmlhttp.responseText;
                  }else {
                    alert("error");
                    oTest.innerHTML = "[无]";
                  }
              }
          }
          xmlhttp.open("get", "get.jsp?id="+input.value,true);
          xmlhttp.send(null);
      }
  </script>
</body>
</html>
