﻿<%@ page language="java" import="java.util.*,java.io.*"
contentType="text/html;charset=utf-8"%>
<!DOCTYPE html>
<html><head><title>Rich Text Editing</title></head>
<script>
	function italic(){
		document.execCommand("italic",false,null);
	}
	function color(){
		document.execCommand("forecolor",false,"red");
	}
	function fontsize(){
		document.execCommand("fontsize",false,7);
	}
	function image(){
		document.execCommand("insertimage",false,"images/home.gif");
	}
	function link(){
		document.execCommand("createlink",false,"www.baidu.com");
	}
	function cancel(){
		document.execCommand("selectall",false,null);
		document.execCommand("removeformat",false,null);
		document.execCommand("unlink",false,null);
		var o = document.getElementById("richedit");
		var imgs=o.getElementsByTagName("img");
		for(var i=0;i<imgs.length;i++){
			o.removeChild(imgs[i]);
		}
		
	}
	function code(){
		var o = document.getElementById("richedit");
		alert(o.innerHTML);
	}
</script>
<body>
	<div id="richedit" contenteditable="true" style="padding: 10px;width: 400px;height: 200px;border: solid 1px black;">
	</div><br>
	<input type="button" value="斜体" onclick="italic()">
	<input type="button" value="颜色" onclick="color()">
	<input type="button" value="字号" onclick="fontsize()">
	<input type="button" value="图像" onclick="image()">
	<input type="button" value="链接" onclick="link()">
	<input type="button" value="撤销" onclick="cancel()">
	<input type="button" value="代码" onclick="code()">
</body>
</html>