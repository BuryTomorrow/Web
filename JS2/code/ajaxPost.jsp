﻿<!DOCTYPE html>
<html>
<head>
  <title>AJAX-post</title>
  <style>
    p {font-size:20px}
  </style>
</head>
<body>
  <h1>Ajax-post</h1>
  <p>id：<input id="t1" type="text"></p>
  <p>姓名：<input id="t2" type="text"></p>
  <p><span id="t4"></span></p>
   <script>
        var p2 = document.getElementById("t2");
        p2.onblur = function () {
            var p1 = document.getElementById("t1");
            var p3 = document.getElementById("t4");
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4) {
                    if (xmlhttp.status >= 200 && xmlhttp.status < 300 || xmlhttp.status >= 304) {
                        p3.innerHTML = xmlhttp.responseText;
                    } else {
                        alert("error");
                    }
                }
            }
            var parm = "id=" + p1.value + "&name=" + encodeURIComponent(p2.value);
            xmlhttp.open("post", "post.jsp?", true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            xmlhttp.send(parm);
        }
    </script>
</body>
</html>
