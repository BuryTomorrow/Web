import java.io.*;
import java.util.*;

class ShowTags{
  public static void main(String[] args)throws IOException{
     String content = readFile(".\\grassland.htm");
	 HashMap<String,Integer> labels = new HashMap<String,Integer>();
	 
	 for(int i=0;i<content.length();){
		 if(content.charAt(i)=='<'){
			 String word="<";
			 for(int j=i+1;j<content.length();j++){
				 char c = content.charAt(j);
				 if(c>='a'&&c<='z'){
					 c-=32;
				 }
				 word = word+c;
				 if(content.charAt(j)==' '||content.charAt(j)=='>'){
					 word = word.substring(0,word.length()-1);
					 word = word +">";
					 i = j+1;
					 Integer count = 1;
					 if(labels.containsKey(word)){
						 count = labels.get(word)+1;
					 }
					 labels.put(word,count);
					 break;
				 }
			 }
		 }else{
			 i++;
		 }
	 }
	
	 int k=0;
	 Iterator <String> it = labels.keySet().iterator();
	 while(it.hasNext()){
		 k++;
		 String key = (String) it.next();
		 Integer count = labels.get(key);
		 System.out.print(key+":"+count+"\t");
		 if(k%3==0) System.out.println();
	 }
  }

  static String readFile(String fileName) throws IOException{
    	StringBuilder sb = new StringBuilder("");
	int c1;
	FileInputStream f1= new FileInputStream(fileName);		
	InputStreamReader in = new InputStreamReader(f1, "GBK");

	while ((c1 = in.read()) != -1) {
	  sb.append((char) c1);
	}        
        return sb.toString();
  }
}


