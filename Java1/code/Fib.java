import java.util.Arrays;
import java.util.*;
public class Fib{
	public static void main(String[] args){
		//쳲�����
		ArrayList<Long> fibs = new ArrayList<Long>();
		Long fib1 = new Long(0);
		Long fib2 = new Long(1);
		Long fib = new Long(1);
		fibs.add(fib1);
		while(fib2<Long.MAX_VALUE&&fib2>0){
			fibs.add(fib2);
			fib = fib2;
			fib2 = fib2+fib1;
			fib1 = fib;
		}
		System.out.println("max fib(Long):"+fibs.get(fibs.size()-1)+"count: "+fibs.size());
		
		//(2)
		double ratio[] = new double [fibs.size()];
		Iterator<Long> it = fibs.iterator();
		double last = 0;
		int i=0;
		while(it.hasNext()){
			double a=(double)(Long)it.next();
			if(i<3) ratio[i++]=a;
			else ratio[i++]=last/a;
			last = a;
		}
		i=0;
		for(double a:ratio){
			i++;
			System.out.print(a+" ");
			if(i%3==0) System.out.println();
		}
	}
}